# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 8 hodin |
| jak se mi to podařilo rozplánovat | V celku dobře, nejdříve příprava schématu a následně výroba|
| design zapojení | https://gitlab.spseplzen.cz/hroudam/projekt-vlastni-shield-michael-hrouda/-/blob/main/dokumentace/design/Design_zapojen%C3%AD.jpg |
| proč jsem zvolil tento design | Protože mi přišel nejednoduší a nejpraktičtější |
| zapojení | https://gitlab.spseplzen.cz/hroudam/projekt-vlastni-shield-michael-hrouda/-/blob/main/dokumentace/design/Design_zapojen%C3%AD.jpg |
| z jakých součástí se zapojení skládá | RGB LED modul 8 x NeoPixel WS2812, Voděodolný teploměr pro jednodeskové počítače DS18B20, Rotační enkodér, Prototyp shield pro Arduino UNO, Kabely, 1x 4,7k ohm resistor, 2x 220 ohm resistor |
| realizace | https://gitlab.spseplzen.cz/hroudam/projekt-vlastni-shield-michael-hrouda/-/blob/main/dokumentace/fotky/IMG_3220.jpg |
| nápad, v jakém produktu vše propojit dohromady| Wemos |
| co se mi povedlo | Rychlost výroby |
| co se mi nepovedlo/příště bych udělal/a jinak | Hezčí design |
| zhodnocení celé tvorby | Celkem ekeftivní 9 / 10 :) |