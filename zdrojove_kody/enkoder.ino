const int CLK = D5;
const int DT = D6;
const int SW = D7;
int encoderValue = 0;
int lastCLKState;
int currentStateCLK;
bool buttonPressed = false;
void setup() {
  pinMode(CLK, INPUT);
  pinMode(DT, INPUT);
  pinMode(SW, INPUT_PULLUP);
  
  lastCLKState = digitalRead(CLK);
  Serial.begin(115200);
}
void loop() {
  currentStateCLK = digitalRead(CLK);
  if (currentStateCLK != lastCLKState) {
    if (digitalRead(DT) != currentStateCLK) {
      encoderValue++;
    } else {
      encoderValue--;
    }
    Serial.print("Encoder Value: ");
    Serial.println(encoderValue);
  }
  lastCLKState = currentStateCLK;
  if (digitalRead(SW) == LOW && !buttonPressed) {
    buttonPressed = true;
    Serial.println("Button Pressed");   
  } else if (digitalRead(SW) == HIGH && buttonPressed) {
    buttonPressed = false;
  }
  delay(1);
}
